# Coding Partner

A web application goaled towards finding other programmers to work on a project together.
This service will be free to use. 

### Target audience: 
- people wanting to learn
- people looking for a side project
- people looking for people to work with on a side project

### How it works: 
People can post their idea for a project, and others can join.
1. People can post their project idea
2. People can request to join a project
3. Project owner can accept/deny join requests
4. Happy programming

### Potential extra:
- forum for people to discuss
- partnering with other learning platforms

Disclaimer: 
_This is primarily based for open source projects, and not goaled towards finding business partners._